import { createSlice } from '@reduxjs/toolkit'

const SAVED_AUTH_KEY = 'crowd-funding-auth'

const loadInitialState = () => {
    let savedState = sessionStorage.getItem(SAVED_AUTH_KEY)
    if (savedState != null) {
        return JSON.parse(savedState)
    }

    return { 
        isAuth: false, 
        userData: {
            token: null,
            userName: null,
            userId: null
        }
    }
}

const initialState = loadInitialState()

const loginAction = (state, loginResult) => {
    state.isAuth = true
    state.userData = {
        token: loginResult.token,
        userName: loginResult.userData.name,
        userId: loginResult.userData.id,
    }

    sessionStorage.setItem(SAVED_AUTH_KEY, JSON.stringify(state))
}

const logoutAction = (state) => {
    state.isAuth = false
    state.userData = {
        token: null,
        userName: null,
        userId: null,
    }

    sessionStorage.removeItem(SAVED_AUTH_KEY)
}

const authSlice = createSlice({
    name: 'auth',
    initialState, 
    reducers: {
        login: (state, action) => loginAction(state, action.payload),
        logout: (state) => logoutAction(state)
    }
})

export const { login, logout } = authSlice.actions

export default authSlice.reducer