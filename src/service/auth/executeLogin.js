import api from '../api'

export const executeLogin = async (loginData) => {
    try {
        let response = await api.post('/users/login', loginData)
        return response.data
    } catch (exception) {
        throw new Error('Dados inválidos para o login!')
    }
}