import { Container } from "@mui/material"
import AppToolbar from "../../components/toolbar"

const AppHomePage = () => {

    return (
        <div>
            <AppToolbar />
            <Container style={{ marginTop: '20px' }}>
                Crowd Funding Home
            </Container>
        </div>
    )
}

export default AppHomePage